#!/bin/bash
#
# Ubuntu Linux installation script for installed version(s) of JModelica
# originally hosted at https://jmodelica.org/
#
# This script is hosted at https://gitlab.com/christiankral/install_jmodelica/.
#
# Version history
# 2020-08-28 Inital version for Ubuntu 18.04 version date 2019-11-23

# Set work directory variable, which may be replaced by ~/work or ~/tmp for
# most users. This directory shall be created before running this script.
export WORK=/work
# Local installation directory with read + write + exectue access of the current
# user. This directory shall be created before running this script.
export INSTALLDIR=~/bin
# Branch to be checked out from JModelica.org,
# see https://trac.jmodelica.org/browser/
export VERSION=20191123

# Install required packages
sudo apt install -y  g++
sudo apt install -y  subversion
sudo apt install -y  gfortran
sudo apt install -y  ipython
sudo apt install -y  cmake
sudo apt install -y  swig
sudo apt install -y  ant
sudo apt install -y  openjdk-8-jdk
sudo apt install -y  python-dev
sudo apt install -y  python-numpy
sudo apt install -y  python-scipy
sudo apt install -y  python-matplotlib
sudo apt install -y  cython
sudo apt install -y  python-lxml
sudo apt install -y  python-nose
sudo apt install -y  python-jpype
sudo apt install -y  zlib1g-dev
sudo apt install -y  libboost-dev
sudo apt install -y  jcc
sudo apt install -y  libblas-dev
sudo apt install -y  libgfortran4
sudo apt install -y  libgfortran-7-dev

# Switch to temp. work directory
cd ${WORK}
git clone https://gitlab.com/christiankral/jmodelica_installed
cd jmodelica_installed
# Unpack selected versions
tar -zxvf jmodelica${VERSION}.tar.gz
tar -zxvf ipopt.tar.gz
# Move directories to install directory
mv jmodelica${VERSION} ${INSTALLDIR}/jmodelica${VERSION}
mv ipopt ${INSTALLDIR}/ipopt

# Create start script
echo -e '#!/bin/bash' >${INSTALLDIR}/jmodelica${VERSION}.sh
echo "export WORK=${WORK}" >>${INSTALLDIR}/jmodelica${VERSION}.sh
echo "cd ${WORK}" >>${INSTALLDIR}/jmodelica${VERSION}.sh
echo "export MODELICAPATH=${WORK}:${INSTALLDIR}/jmodelica${VERSION}/ThirdParty/MSL/" >>${INSTALLDIR}/jmodelica${VERSION}.sh
echo "export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64/" >>${INSTALLDIR}/jmodelica${VERSION}.sh
echo "export JRE_HOME=/usr/lib/jvm/java-8-openjdk-amd64/jre/" >>${INSTALLDIR}/jmodelica${VERSION}.sh
echo "export J2SDKDIR=/usr/lib/jvm/java-8-openjdk-amd64/" >>${INSTALLDIR}/jmodelica${VERSION}.sh
echo "export J2REDIR=/usr/lib/jvm/java-8-openjdk-amd64/jre/" >>${INSTALLDIR}/jmodelica${VERSION}.sh
echo "${INSTALLDIR}/jmodelica${VERSION}/bin/jm_python.sh" >>${INSTALLDIR}/jmodelica${VERSION}.sh
chmod +x ${INSTALLDIR}/jmodelica${VERSION}.sh

ln -sf ${INSTALLDIR}/jmodelica${VERSION}.sh ${INSTALLDIR}/jmodelica.sh

echo ""
echo "Installation completed"
echo "Start script is: ${INSTALLDIR}/jmodelica.sh"
echo "Being an alias of: ${INSTALLDIR}/jmodelica${VERSION}.sh"
